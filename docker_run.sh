#!/bin/bash

ARGS="--bind 0.0.0.0:5000 homepage:app"

if [ "$DOCKER_PRODUCTION" ]; 
    then ARGS="--log-file /home/homepage/gunicorn.log $ARGS";
    else ARGS="--capture-output $ARGS";
fi;

gunicorn $ARGS;