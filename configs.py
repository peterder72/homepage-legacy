import os
import dotenv

basedir = os.path.abspath(os.path.dirname(__file__))

# TODO add proper environment stuff


class Config():

    dotenv.load_dotenv('homepage.env')
    dotenv.load_dotenv('email.env')

    SMTP_ADDR = os.environ.get('SMTP_ADDR')
    SMTP_PASSWD = os.environ.get('SMTP_PASSWD')
    SMTP_SERVER = os.environ.get('SMTP_SERVER')
    SMTP_PORT = os.environ.get('SMTP_PORT')

    HOMEPAGE_EMAIL = True

    SECRET_KEY = os.environ.get(
        'SECRET_KEY') or 'abcbabcbabsdhjaksdl129381029aposdklckzx;clqwee!@#'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    MAX_CONTENT_LENGTH = 20 * 1024 * 1024
    UPLOAD_FOLDER = 'uploads'


class DevConfig(Config):

    SQLALCHEMY_DATABASE_URI = "sqlite:///:memory:"
    HOMEPAGE_EMAIL = False


class TestConfig(Config):

    SQLALCHEMY_DATABASE_URI = "sqlite:///:memory:"
    TESTING = True
    HOMEPAGE_EMAIL = False
    PRESERVE_CONTEXT_ON_EXCEPTION = False


class ProdConfig(Config):

    dotenv.load_dotenv('postgres.env')
    SQLALCHEMY_DATABASE_URI = "postgres://{}:{}@db:5432/{}".format(
        os.environ.get("POSTGRES_USER"), os.environ.get(
            "POSTGRES_PASSWORD"), os.environ.get("POSTGRES_DB")
    )
    UPLOAD_FOLDER = '/srv/homepage/uploads'
    PRODUCTION = True


class MigrationConfig(Config):

    dotenv.load_dotenv('postgres.env')
    SQLALCHEMY_DATABASE_URI = "postgres://{}:{}@localhost:54320/{}".format(
        os.environ.get("POSTGRES_USER"), os.environ.get(
            "POSTGRES_PASSWORD"), os.environ.get("POSTGRES_DB")
    )


config = {
    'debug': DevConfig,
    'production': ProdConfig,
    'migration': MigrationConfig,
    'testing': TestConfig
}
