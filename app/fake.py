from random import randint
from sqlalchemy.exc import IntegrityError
from faker import Faker
from . import db
from .models import User, Project


def gen_users(count=100):
    fake = Faker()
    i = 0
    while i < count:
        u = User(email=fake.email(),
                 username=fake.user_name(),
                 password='password',
                 confirmed=True,
                 name=fake.name(),
                 joined=fake.past_date())
        db.session.add(u)
        try:
            db.session.commit()
            i += 1
        except IntegrityError:
            db.session.rollback()


def gen_projects(count=100):
    fake = Faker()
    user_count = User.query.count()
    for i in range(count):
        u = User.query.offset(randint(0, user_count - 1)).first()
        p = Project(body='\n'.join([fake.text() for _ in range(5)]),
                    title=' '.join(fake.text().split(" ")[:2]),
                    status=fake.random_int(0, 5),
                    start_date=fake.past_date(),
                    uid=u.id)
        db.session.add(p)
    db.session.commit()
