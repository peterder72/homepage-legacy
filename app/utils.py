import os

from flask import current_app
from werkzeug.utils import secure_filename

from app.security import check_picture


def upload_picture(pic):

    if check_picture(pic.stream.read()):

        fname = pic.filename
        upload_folder = current_app.config.get('UPLOAD_FOLDER')

        if os.access(os.path.join(upload_folder, fname), os.F_OK):
            ext = '.'.join(pic.filename.split('.')[1:])
            name = pic.filename.split('.')[0]
            c = 1
            while os.access(os.path.join(upload_folder, f'{name}_{c}.{ext}'), os.F_OK):
                c += 1
            fname = f'{name}_{c}.{ext}'

        pic.save(os.path.join(upload_folder, secure_filename(fname)))

        return True

    return False
