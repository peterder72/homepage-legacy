from flask import render_template, request, abort
from app.main import main


@main.app_errorhandler(404)
def notFound(e):

    xss = False
    something = None

    if '>' in request.path and '<' in request.path:
        xss = True

    if request.path.startswith('/profile/'):
        something = 'profile'

    if request.path.startswith('/projects/'):
        something = 'project'

    return render_template('404.html', url=request.path, xss=xss, something=something), 404


@main.app_errorhandler(500)
def internalError(e):

    return render_template('500.html'), 500


@main.app_errorhandler(501)
def not_implemented_error(e):

    return render_template('501.html'), 501


@main.app_errorhandler(403)
def forbidden(e):

    return render_template('403.html'), 403


@main.app_errorhandler(401)
def auth(e):

    return render_template('401.html'), 401


@main.app_errorhandler(400)
def badreq(e):

    return render_template('400.html'), 400


@main.route('/501')
def test_not_implemented():
    abort(501)


@main.route('/500')
def test_internal():
    abort(500)


@main.route('/400')
def test_badreq():
    abort(400)


@main.route('/403')
def test_forbidden():
    abort(403)
