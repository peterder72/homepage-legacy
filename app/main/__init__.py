from flask import Blueprint

main = Blueprint('main', __name__)

from . import email as email_worker
from . import views, errors, auth
