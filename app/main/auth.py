import re
import time

from flask import current_app, flash, g, redirect, request, session, url_for
from flask_login import login_user, logout_user

from app import db, forms
from app.main import main
from app.models import User
from app.regex import email_reg

from . import email_worker


@main.before_app_request
def injectForms():
    g.login_form = forms.LoginForm()
    g.signup_form = forms.SignUpForm()

    if request.form.get('form_type') == 'login':
        if g.login_form.validate_on_submit():

            if re.match(email_reg, g.login_form.email.data):
                u = User.query.filter_by(
                    email_plain=g.login_form.email.data
                ).first()
            else:
                u = User.query.filter_by(
                    username=g.login_form.email.data
                ).first()

            if u is not None and u.verify_password(g.login_form.password.data):
                flash("Congratulations! You are signed in now!", "success")
                login_user(u)
            else:
                if u is None:
                    flash("User does not exist", 'error-login')
                else:
                    flash("Wrong email/password")

                return redirect(request.path + "#modalLoginForm")

            return redirect(request.path + '#clear')

        else:
            for fieldName, errorMessages in g.signup_form.errors.items():
                for err in errorMessages:
                    flash("Error for {}: {}".format(
                        fieldName, err), 'error-login')
            return redirect(request.path + '#modalLoginForm')

    elif request.form.get('form_type') == 'signup':
        if g.signup_form.validate_on_submit():
            user = User(
                email=g.signup_form.email.data,
                username=g.signup_form.username.data,
                password=g.signup_form.password.data,
                last_sent_conf=time.time(),
                name=g.signup_form.name.data,
                surname=g.signup_form.surname.data
            )

            db.session.add(user)
            db.session.commit()

            if current_app.config.get('HOMEPAGE_EMAIL'):
                mail = email_worker.ConfirmEmail(user)
                mail.send()

            login_user(user)
            session['new_user'] = user.id

            return redirect(url_for("main.success") + "#clear")

        else:
            for fieldName, errorMessages in g.signup_form.errors.items():
                for err in errorMessages:
                    flash("Error for {}: {}".format(
                        fieldName, err), 'error-signup')
            return redirect(request.path + '#modalSignupForm')


@main.route('/signout')
def signout():
    logout_user()
    flash("Signed out")
    return redirect(url_for('.index'))
