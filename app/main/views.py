
import jwt.exceptions
from flask import (abort, redirect, render_template,
                   request, session, url_for)

from flask_login import login_required

from app import db
from app.main import email_worker, main
from app.models import User, Permission
from app.utils import upload_picture
from app.security import permissions_required, under_construction


@main.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')


@main.route('/copyright')
def copyright():
    return render_template('copyright.html')


@main.route('/admin')
def rickroll():
    return redirect('https://www.youtube.com/watch?v=dQw4w9WgXcQ')


@main.route('/about')
def about_page():

    return render_template('about.html')

# TODO uploads
@main.route('/upload', methods=['POST'])
@under_construction
@login_required
def upload_new():

    if 'pic' not in request.files:
        abort(400)

    file = request.files['pic']
    if file.filename == '':
        abort(400)

    if upload_picture(file):
        return 'ok'
    else:
        abort(400)


@main.route("/success")
def success():
    if not session.get('new_user'):
        return redirect(url_for('.index'))

    user = User.query.filter_by(id=session['new_user']).first()

    del session['new_user']

    if not user:
        abort(400)

    return render_template('success.html', user=user)


@main.route("/confirm/<token>")
def confirm_email(token):
    try:
        uid = email_worker.ConfirmEmail.check(token)
    except jwt.exceptions.PyJWTError:
        abort(400)
    else:
        user = User.query.filter_by(id=uid).first()
        if not user:
            abort(400)

        user.confirmed = True
        db.session.commit()

        session['new_user'] = user.id

        return redirect(url_for('.confirmed'))


@main.route('/manage')
@login_required
@permissions_required(Permission.ADMIN)
def manage():
    return render_template('manage.html', users=User.query.all())


@main.route('/confirmed')
def confirmed():
    if not session.get('new_user'):
        return redirect(url_for('.index'))

    user = User.query.filter_by(id=session['new_user']).first()
    if not user:
        abort(400)

    return render_template('confirmed.html', user=user)


@main.route('/profile/<username>')
def profile(username):
    u = User.query.filter_by(username=username).first()

    if not u:
        abort(404)

    return render_template('profile.html', user=u)
