import base64
import smtplib
import time
from abc import ABC, abstractmethod
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import jwt
from flask import current_app, render_template

from app.models import User


class Email(ABC):

    @abstractmethod
    def send(self):
        pass

    @staticmethod
    def sendMultipart(msg):

        passwd = current_app.config.get("SMTP_PASSWD")
        remote_server = current_app.config.get("SMTP_SERVER")
        port = current_app.config.get("SMTP_PORT")
        src = current_app.config.get("SMTP_ADDR")

        server = smtplib.SMTP_SSL(remote_server, port)
        server.set_debuglevel(False)
        server.ehlo()
        server.login(src, passwd)
        server.send_message(msg)
        server.quit()


class ConfirmEmail(ABC):

    text_body = '''Greetings!
    Click this url to confirm your address: {}/confirm/{}
    '''

    def __init__(self, user: User):

        self.user = user

    def send(self):

        src = current_app.config.get("SMTP_ADDR")

        token = jwt.encode(
            {
                'sub': self.user.id,
                'iat': time.time(),
                'exp': time.time() + 60 * 60 * 60,
                'action': 'verify',
                'email': self.user.email
            },
            current_app.config.get('SECRET_KEY'), algorithm='HS256')

        token = base64.b64encode(token)

        msg = MIMEMultipart()
        msg['From'] = src
        msg['To'] = self.user.email
        msg['Subject'] = 'Osetr.su confirmation'

        root = 'http://localhost:5000' if current_app.debug else "https://osetr.su"

        msg.attach(
            MIMEText(render_template('email.html', root=root, token=token.decode('utf-8')), 'html', 'utf-8')
        )

        Email.sendMultipart(msg)

    @staticmethod
    def check(token: str):

        token = base64.b64decode(token)

        data = jwt.decode(
            token, current_app.config.get('SECRET_KEY'), algorithms=['HS256']
        )

        if data['action'] != 'verify':
            raise jwt.InvalidTokenError()

        return data['sub']
